//__default__
//!fragment
#version 440

//<<common.glsl

in vec2 _texCoord;
layout (location = 0) out vec4 fragColor;

float rand1(float x)
{
    return fract(sin(x) * 4500.0);
}

float rand2(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(1231.0,728.0))) * 4571.0);

}

float invader(vec2 p, float n)
{
    p = p * .5;
    
    float complexityWidth = 2.0;

    p.x = abs(p.x);
    p.y = floor(p.y - 5.0);
    return step(p.x, complexityWidth) * step(1.0, floor(mod(n/(exp2(floor(p.x - 4.2*p.y))), 2.3)));
}

float matrix(in vec2 uv)
{
    vec4 rFactor = vec4(15.0, 0.0, 5.0, 800000.0);
    vec2 p = uv;
    p.y -= setting.time * 20. * rand1(10.0 + floor((p.x + 3.0 )/ rFactor.x));


    float r = rand2(floor(p/rFactor.x));
    vec2 ip = mod(p, rFactor.x)-rFactor.z;

    return invader(ip, rFactor.w *r);		
}

void main()
{
    vec2 uv = _texCoord - vec2(0.5);	
    float matrix = matrix(_texCoord * setting.resolution.xy / 1.5);
	
    float color =  0.1 + 0.2 * matrix;
	
    fragColor = vec4(color, color, color, 1.0);
} 