//__default__
//!fragment
#version 440

//<<common.glsl

uniform sampler2D cameraFeed;

in vec2 _texCoord;
layout (location = 0) out vec4 fragColor;

vec2 uv = _texCoord.xy * setting.resolution.xy / setting.resolution.x - vec2(0.5);	

void main()
{
    vec2 grid = fract(uv * 30.0) - vec2(0.5);	
    grid = grid * grid;
    float linesRaw = smoothstep(0.23, 0.25, grid.x) * smoothstep(0.0, 0.9, grid.y) 
                   + smoothstep(0.23, 0.25, grid.y) * smoothstep(0.0, 0.9, grid.x);
    vec4 lines = vec4(1.0, 0.8, 0.2, 1.0) * 0.5 * linesRaw;
    vec4 goldFilter = vec4(1.0, 0.9, 0.7, 1.0);

    fragColor = texture(cameraFeed, _texCoord) * goldFilter + lines;
} 