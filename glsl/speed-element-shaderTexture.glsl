//__default__
//!fragment
#version 440

//<<common.glsl

#define PI 3.1415
#define LINE_AMOUNT_FACTOR 60.
#define LINE_THICKNESS 0.2
#define LINE_WIDTH 0.008
#define LINE_RADIUS 0.60
#define INDICATOR_RANGE 2.

in vec2 _texCoord;
layout (location = 0) out vec4 fragColor;

uniform vec2 wheelSpeed;

vec4 color = vec4(0.3, 0.3, 0.3, 1.0);
vec4 accentColor = vec4(0.3, 0.8, 0.3, 1.0);

float BackgroundBars(in vec2 uv)
{
    float lines = abs(fract(uv.y * LINE_AMOUNT_FACTOR) - 0.5);  
    lines = (1.0 - smoothstep(LINE_THICKNESS, LINE_THICKNESS + 0.15, lines));   
    lines *= (1.0 - smoothstep(LINE_WIDTH, LINE_WIDTH + 0.006, abs(uv.x - LINE_RADIUS)));

    float startAngleRight = (10. - INDICATOR_RANGE)/20. * PI;
    float stopAngleRight = (10. + INDICATOR_RANGE)/20. * PI;

    float startAngleLeft = -(10. + INDICATOR_RANGE)/20. * PI;
    float stopAngleLeft = -(10. - INDICATOR_RANGE)/20. * PI;

    lines *= step(startAngleRight, uv.y) * ( 1. - step(stopAngleRight, uv.y)) + 
             step(startAngleLeft, uv.y) * ( 1. - step(stopAngleLeft, uv.y));

    return lines;
}

float Bars( in vec2 uv, in float left, in float right )
{
    float lines = abs(fract(uv.y * LINE_AMOUNT_FACTOR) - 0.5);  
    lines = (1.0 - smoothstep(LINE_THICKNESS, LINE_THICKNESS + 0.15, lines));   
    lines *= (1.0 - smoothstep(LINE_WIDTH, LINE_WIDTH + 0.006, abs(uv.x - LINE_RADIUS)));

    float startAngleRight = (10. - INDICATOR_RANGE * max(0.0, right))/20. * PI;
    float stopAngleRight = (10. - INDICATOR_RANGE * min(0.0, right))/20. * PI;

    float startAngleLeft = (-10. + INDICATOR_RANGE * min(0.0, left))/20. * PI;
    float stopAngleLeft = (-10. + INDICATOR_RANGE * max(0.0, left))/20. * PI;

    lines *= step(startAngleRight, uv.y) * ( 1. - step(stopAngleRight, uv.y)) + 
             step(startAngleLeft, uv.y) * ( 1. - step(stopAngleLeft, uv.y));

    return lines;
}

float BarsGlow( in vec2 uv, in float left, in float right, float glowFactor )
{
    float lines = abs(fract(uv.y * LINE_AMOUNT_FACTOR) - 0.5);  
    lines = (1.0 - smoothstep(LINE_THICKNESS*2., LINE_THICKNESS*2. + 0.5, lines));   
    lines *= (1.0 - smoothstep(LINE_WIDTH*2., LINE_WIDTH*2. + glowFactor, abs(uv.x - LINE_RADIUS)));

    float startAngleRight = (10. - INDICATOR_RANGE * max(0.0, right))/20. * PI;
    float stopAngleRight = (10. - INDICATOR_RANGE * min(0.0, right))/20. * PI;

    float startAngleLeft = (-10. + INDICATOR_RANGE * min(0.0, left))/20. * PI;
    float stopAngleLeft = (-10. + INDICATOR_RANGE * max(0.0, left))/20. * PI;

    lines *= smoothstep(startAngleRight - glowFactor, startAngleRight, uv.y) * 
            ( 1. - smoothstep(stopAngleRight, stopAngleRight + glowFactor, uv.y)) * pow(abs(right), 0.2) + 
             smoothstep(startAngleLeft - glowFactor, startAngleLeft, uv.y) * 
             ( 1. - smoothstep(stopAngleLeft, stopAngleLeft + glowFactor, uv.y)) * pow(abs(left), 0.2);

    return lines ;
}

vec2 Cartesian2Polar(in vec2 uv)
{
    return vec2(length(uv), atan(uv.x, uv.y));
}

void main( void ) 
{
    vec2 uv  = 2.0 * _texCoord.xy - 1.0;
    vec2 coord = uv;
    coord = Cartesian2Polar(uv);

    float highlightedBars = Bars(coord, wheelSpeed.x, wheelSpeed.y);
    float highlightedBarsGlow = BarsGlow(coord, wheelSpeed.x, wheelSpeed.y, 0.1);
    float backgroundBars = BackgroundBars(coord) * (1.0 - step(0.0001, highlightedBars));
    vec4 base = accentColor * highlightedBars 
            + 0.3 * accentColor * highlightedBarsGlow 
            + 0.5 * color * backgroundBars;

    fragColor = base;
}
