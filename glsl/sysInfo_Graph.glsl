//__default__
//!fragment
#version 440

//<<common.glsl
//<<common_Graph.glsl

layout( location = 0 ) out vec4 fragColor;
in vec2 _texCoord;

vec2  pixelSize           = 1.0 / setting.resolution;
float dataCoordResolution = 1.0 / float( graph.numberOfValues );

void
main()
{
    int positionInGraph = int( _texCoord.x / dataCoordResolution );
    float yPosition = mix(graph.yRange.y, graph.yRange.x, _texCoord.y);
    float range = abs(graph.yRange.y - graph.yRange.x);

    float d = distance( yPosition, graph.values[positionInGraph] ) / range; 
    float alpha = smoothstep(0.0, 3 * pixelSize.y, d);

    fragColor = mix(setting.colorAccent, setting.color, alpha);
}

//__cpu__
//!fragment
#version 440

//<<common.glsl
//<<common_Graph.glsl

layout( location = 0 ) out vec4 fragColor;
in vec2 _texCoord;

vec2  pixelSize           = 1.0 / setting.resolution;
float dataCoordResolution = 1.0 / float( graph.numberOfValues );

void
main()
{
    int positionInGraph = int( _texCoord.x / dataCoordResolution );
    float yPosition = mix(graph.yRange.y, graph.yRange.x, _texCoord.y);
    float range = abs(graph.yRange.y - graph.yRange.x);

    float d = ( yPosition - graph.values[positionInGraph] ) / range; 
    float alpha = smoothstep(0.0, 3 * pixelSize.y, d);

    fragColor = mix(setting.colorAccent, setting.color, alpha);
}
