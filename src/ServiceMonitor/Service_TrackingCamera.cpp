#include "ServiceMonitor/Service_TrackingCamera.hpp"

#include "GuiGL/elWidget_Label.hpp"
#include "common/Helplets.hpp"
#include "serviceData/trackingCamera_t.hpp"

using namespace el3D;
using namespace common;

namespace ServiceMonitor
{
Service_TrackingCamera::Service_TrackingCamera(
    const Service_CTor& args ) noexcept
    : Service_base( args )
{
    this->distanceFromOrigin =
        this->header->Add< GuiGL::elWidget_Label >( "Value" );
    this->distanceFromOrigin->SetPositionPointOfOrigin(
        GuiGL::POSITION_TOP_RIGHT );
    this->distanceFromOrigin->SetText( "0.0 m" );

    this->position.SetUp( this->infoFrame, "pos", common::ELEMENT_SPACING );
    this->rotation.SetUp( this->infoFrame, "rot", this->position.key.Get() );
    this->velocity.SetUp( this->infoFrame, "speed", this->rotation.key.Get() );
    this->acceleration.SetUp( this->infoFrame, "accel",
                              this->velocity.key.Get() );
    this->angularVelocity.SetUp( this->infoFrame, "°speed",
                                 this->acceleration.key.Get() );
    this->angularAcceleration.SetUp( this->infoFrame, "°accel",
                                     this->angularVelocity.key.Get() );
    this->trackerConfidence.SetUp( this->infoFrame, "tracker confidence",
                                   this->angularAcceleration.key.Get() );
    this->mapperConfidence.SetUp( this->infoFrame, "mapper confidence",
                                  this->trackerConfidence.key.Get() );

    this->infoFrame->SetSize( { this->infoFrame->GetSize().x,
                                this->mapperConfidence.key->GetPosition().y +
                                    this->mapperConfidence.key->GetSize().y +
                                    common::ELEMENT_SPACING } );
}

void
Service_TrackingCamera::Update() noexcept
{
    Service_base::Update();

    if ( this->IsConnected() && this->subscriber.hasSamples() )
    {
        this->subscriber.take().and_then( [&]( iox::popo::Sample< const void >&
                                                   chunk ) {
            const serviceData::trackingCamera_t* d =
                reinterpret_cast< const serviceData::trackingCamera_t* >(
                    chunk.get() );

            auto x = d->translation.x.GetMeter();
            auto y = d->translation.y.GetMeter();
            auto z = d->translation.z.GetMeter();

            double distanceFromStart = sqrt( x * x + y * y + z * z );

            this->distanceFromOrigin->SetText(
                DoubleToString( distanceFromStart, 3 ) + " m" );


            this->position.value->SetText(
                DoubleToString( d->translation.x.GetMeter(), VALUE_ACCURACY ) +
                ", " +
                DoubleToString( d->translation.y.GetMeter(), VALUE_ACCURACY ) +
                ", " +
                DoubleToString( d->translation.z.GetMeter(), VALUE_ACCURACY ) );

            this->rotation.value->SetText(
                DoubleToString( d->rotation.x, VALUE_ACCURACY, true ) + ", " +
                DoubleToString( d->rotation.y, VALUE_ACCURACY, true ) + ", " +
                DoubleToString( d->rotation.z, VALUE_ACCURACY, true ) + ", " +
                DoubleToString( d->rotation.w, VALUE_ACCURACY, true ) );

            this->velocity.value->SetText(
                DoubleToString( d->velocity.x.GetMeterPerSecond(),
                                VALUE_ACCURACY, true ) +
                ", " +
                DoubleToString( d->velocity.y.GetMeterPerSecond(),
                                VALUE_ACCURACY, true ) +
                ", " +
                DoubleToString( d->velocity.z.GetMeterPerSecond(),
                                VALUE_ACCURACY, true ) );

            this->acceleration.value->SetText(
                DoubleToString( d->acceleration.x.GetMeterPerSecondSquare(),
                                VALUE_ACCURACY, true ) +
                ", " +
                DoubleToString( d->acceleration.y.GetMeterPerSecondSquare(),
                                VALUE_ACCURACY, true ) +
                ", " +
                DoubleToString( d->acceleration.z.GetMeterPerSecondSquare(),
                                VALUE_ACCURACY, true ) );

            this->angularVelocity.value->SetText(
                DoubleToString( d->angularVelocity.x.GetDegreePerSecond(),
                                VALUE_ACCURACY, true ) +
                ", " +
                DoubleToString( d->angularVelocity.y.GetDegreePerSecond(),
                                VALUE_ACCURACY, true ) +
                ", " +
                DoubleToString( d->angularVelocity.z.GetDegreePerSecond(),
                                VALUE_ACCURACY, true ) );

            this->angularAcceleration.value->SetText(
                DoubleToString(
                    d->angularAcceleration.x.GetRadiansPerSecondSquare(),
                    VALUE_ACCURACY, true ) +
                ", " +
                DoubleToString(
                    d->angularAcceleration.y.GetRadiansPerSecondSquare(),
                    VALUE_ACCURACY, true ) +
                ", " +
                DoubleToString(
                    d->angularAcceleration.z.GetRadiansPerSecondSquare(),
                    VALUE_ACCURACY, true ) );

            this->trackerConfidence.value->SetText(
                DoubleToString( d->trackerConfidence.Get(), VALUE_ACCURACY ) );
            this->mapperConfidence.value->SetText(
                DoubleToString( d->mapperConfidence.Get(), VALUE_ACCURACY ) );
        } );
    }
}
} // namespace ServiceMonitor

