#include "ServiceMonitor/Service_Drive.hpp"

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Label.hpp"
#include "serviceData/drive_t.hpp"

using namespace el3D;

namespace ServiceMonitor
{
Service_Drive::Service_Drive( const Service_CTor& args ) : Service_base( args )
{
    this->currentSpeed = this->header->Add< GuiGL::elWidget_Label >();
    this->currentSpeed->SetClassName( "Value", false );
    this->currentSpeed->SetPositionPointOfOrigin(
        GuiGL::positionCorner_t::POSITION_TOP_RIGHT );
    this->currentSpeed->SetText( "[ 0, 0 ]" );
}

void
Service_Drive::Update() noexcept
{
    Service_base::Update();

    if ( this->IsConnected() && this->subscriber.hasSamples() )
    {
        this->subscriber.take().and_then(
            [&]( iox::popo::Sample< const void >& chunk ) {
                const serviceData::drive_t* d =
                    reinterpret_cast< const serviceData::drive_t* >(
                        chunk.get() );

                this->currentSpeed->SetText( "[ " + std::to_string( d->left ) +
                                             ", " + std::to_string( d->right ) +
                                             " ]" );
            } );
    }
}
} // namespace ServiceMonitor
