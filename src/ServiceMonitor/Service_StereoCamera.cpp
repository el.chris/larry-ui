#include "ServiceMonitor/Service_StereoCamera.hpp"

#include "GuiGL/elWidget_Label.hpp"
#include "common/Helplets.hpp"
#include "serviceData/stereoCamera_t.hpp"

using namespace el3D;
using namespace common;

namespace ServiceMonitor
{
Service_StereoCamera::Service_StereoCamera( const Service_CTor& args ) noexcept
    : Service_base( args )
{
    this->distanceToCenter =
        this->header->Add< GuiGL::elWidget_Label >( "Value" );
    this->distanceToCenter->SetPositionPointOfOrigin(
        GuiGL::POSITION_TOP_RIGHT );
    this->distanceToCenter->SetText( "0" );
}

void
Service_StereoCamera::Update() noexcept
{
    Service_base::Update();

    if ( this->IsConnected() && this->subscriber.hasSamples() )
    {
        this->subscriber.take().and_then(
            [&]( iox::popo::Sample< const void >& chunk ) {
                const serviceData::stereoCamera_t* c =
                    reinterpret_cast< const serviceData::stereoCamera_t* >(
                        chunk.get() );
                this->distanceToCenter->SetText(
                    DoubleToString( c->distanceToCenter, 2 ) + "m" );
            } );
    }
}
} // namespace ServiceMonitor
