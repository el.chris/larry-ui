#include "ServiceMonitor/Service_LedControl.hpp"

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Label.hpp"
#include "serviceData/ledControl_t.hpp"

using namespace el3D;

namespace ServiceMonitor
{
Service_LedControl::Service_LedControl( const Service_CTor& args )
    : Service_base( args )
{
    this->lights = this->header->Add< GuiGL::elWidget_Label >();
    this->lights->SetClassName( "Value", false );
    this->lights->SetPositionPointOfOrigin(
        GuiGL::positionCorner_t::POSITION_TOP_RIGHT );
    this->lights->SetText( " . . . " );
}

void
Service_LedControl::Update() noexcept
{
    Service_base::Update();

    if ( this->IsConnected() && this->subscriber.hasSamples() )
    {
        this->subscriber.take().and_then(
            [&]( iox::popo::Sample< const void >& chunk ) {
                const serviceData::ledControl_t* d =
                    reinterpret_cast< const serviceData::ledControl_t* >(
                        chunk.get() );

                std::string state = ( d->red ) ? " R" : " .";
                state += ( d->green ) ? " G" : " .";
                state += ( d->blue ) ? " B" : " .";

                this->lights->SetText( state );
            } );
    }
}
} // namespace ServiceMonitor
