#include "ServiceMonitor/Service_TrackingSensor.hpp"

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Label.hpp"
#include "serviceData/trackingSensor_t.hpp"

using namespace el3D;

namespace ServiceMonitor
{
Service_TrackingSensor::Service_TrackingSensor(
    const Service_CTor& args ) noexcept
    : Service_base( args )
{
    this->state = this->header->Add< GuiGL::elWidget_Label >();
    this->state->SetClassName( "Value", false );
    this->state->SetPositionPointOfOrigin(
        GuiGL::positionCorner_t::POSITION_TOP_RIGHT );
    this->state->SetText( " . . . . " );
}

void
Service_TrackingSensor::Update() noexcept
{
    Service_base::Update();

    if ( this->IsConnected() && this->subscriber.hasSamples() )
    {
        this->subscriber.take().and_then(
            [&]( iox::popo::Sample< const void >& chunk ) {
                const serviceData::trackingSensor_t* d =
                    reinterpret_cast< const serviceData::trackingSensor_t* >(
                        chunk.get() );

                std::string s = ( d->left ) ? " X" : " .";
                s += ( d->centerLeft ) ? " X" : " .";
                s += ( d->centerRight ) ? " X" : " .";
                s += ( d->right ) ? " X" : " .";

                this->state->SetText( s );
            } );
    }
}
} // namespace ServiceMonitor
