#include "3Delch/3Delch.hpp"
#include "Control/Control.hpp"
#include "GuiGL/elWidget_Button.hpp"
#include "HeadsUpDisplay/HeadsUpDisplay.hpp"
#include "LarryScanner/LarryScanner.hpp"
#include "OpenGL/elGeometricObject_InstancedObject.hpp"
#include "ServiceMonitor/ServiceMonitor.hpp"
#include "ServiceMonitor/Service_Camera.hpp"
#include "ServiceMonitor/Service_Drive.hpp"
#include "ServiceMonitor/Service_LedControl.hpp"
#include "ServiceMonitor/Service_SystemMonitor.hpp"
#include "ServiceMonitor/Service_TrackingSensor.hpp"
#include "ServiceMonitor/Service_UltraSonicSensor.hpp"
#include "SystemOverview/SystemOverview.hpp"
#include "World/World.hpp"
#include "iceoryx_posh/internal/roudi/roudi.hpp"
#include "iceoryx_posh/popo/subscriber.hpp"
#include "iceoryx_posh/roudi/iceoryx_roudi_components.hpp"
#include "iceoryx_posh/runtime/posh_runtime_single_process.hpp"
#include "serviceData/trackingCamera_t.hpp"

#include <optional>

using namespace el3D;

bool                                            enableEmbeddedRouDi{ true };
std::optional< SystemOverview::SystemOverview > systemOverview;
std::optional< ServiceMonitor::ServiceMonitor > serviceMonitor;
std::optional< Control::Control >               controller;
std::optional< HeadsUpDisplay::HeadsUpDisplay > headsUpDisplay;
std::optional< World::World >                   world;
el3D::GuiGL::elWidget_base*                     mainHUD{ nullptr };

void
ConfigureLogging()
{
    static logging::output::ToTerminal< logging::threading::InSeparateThread >
        consoleLog;
    consoleLog.SetFormatting( logging::format::DefaultColored );
    consoleLog.SetLogLevel( logging::ERROR );

    static logging::output::ToFile< logging::threading::InSeparateThread >
        fileLog( "larry-ui.log" );
    fileLog.SetFormatting( logging::format::Detailled );
    fileLog.SetLogLevel( logging::INFO );

    logging::elLog::CreateChannel( "larry-ui" );

    logging::elLog::AddOutput( 0, &consoleLog );
    logging::elLog::AddOutput( 0, &fileLog );

    // logging::elLog::EnableOutputRedirection();

    iox::log::LogManager::GetLogManager().SetDefaultLogLevel(
        iox::log::LogLevel::kWarn );
}

void
StartRouDi()
{
    if ( !enableEmbeddedRouDi ) return;

    iox::RouDiConfig_t defaultRouDiConfig = iox::RouDiConfig_t().setDefaults();
    static iox::roudi::IceOryxRouDiComponents roudiComponents(
        defaultRouDiConfig );
    static iox::roudi::RouDi roudi(
        roudiComponents.m_rouDiMemoryManager, roudiComponents.m_portManager,
        iox::roudi::RouDi::RoudiStartupParameters{
            iox::roudi::MonitoringMode::OFF, false } );
    static iox::runtime::PoshRuntimeSingleProcess runtime( "larry-ui" );
}

void
ParseCommandLineArguments( int argc, char* argv[] )
{
    for ( int i = 1; i < argc; ++i )
        if ( std::string( argv[i] ) == "disableEmbeddedRouDi" )
            enableEmbeddedRouDi = false;
}

void
ConnectToRobot(
    const LarryScanner::LarryScanner::connectionInfo_t& connectionInfo )
{
    world->Connect( connectionInfo.domain, connectionInfo.entity );
    headsUpDisplay->Connect( connectionInfo.domain, connectionInfo.entity );
    systemOverview->Connect( connectionInfo.domain, connectionInfo.entity );
    controller->Connect( connectionInfo.domain, connectionInfo.entity );
    serviceMonitor.emplace( mainHUD, connectionInfo.domain,
                            connectionInfo.entity );

    for ( auto& s : connectionInfo.services )
        serviceMonitor->AddService( s );
}

void
DisconnectRobot()
{
    serviceMonitor.reset();
    world->Disconnect();
    controller->Disconnect();
    systemOverview->Disconnect();
    headsUpDisplay->Disconnect();
}

int
main( int argc, char* argv[] )
{
    ParseCommandLineArguments( argc, argv );

    ConfigureLogging();
    StartRouDi();
    iox::runtime::PoshRuntime::initRuntime( "larry-ui" );

    _3Delch::_3Delch::Init( "cfg/larryConfig.lua" );
    auto  instance = _3Delch::_3Delch::GetInstance();
    auto& scene    = instance->GetSceneManager().CreateScene( "main" ).value();

    mainHUD = scene->GUI()->Create< GuiGL::elWidget_base >();
    mainHUD->SetBorderSize( { 0, 0, 0, 0 } );
    mainHUD->SetStickToParentSize( { 0, 0 } );
    mainHUD->SetIsResizable( false );
    mainHUD->SetIsMovable( false );

    headsUpDisplay.emplace( mainHUD, scene.get() );
    systemOverview.emplace( mainHUD );
    LarryScanner::LarryScanner larryScanner( mainHUD, ConnectToRobot,
                                             DisconnectRobot );

    world.emplace( *scene );

    controller.emplace();

    instance->AddMainLoopCallback( [&] {
        larryScanner.Update();
        if ( serviceMonitor ) serviceMonitor->Update();
        controller->Update();
        systemOverview->Update();
        headsUpDisplay->Update();
    } );

    instance->StartMainLoop();
}
