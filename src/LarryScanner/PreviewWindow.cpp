#include "LarryScanner/PreviewWindow.hpp"

#include "GuiGL/elWidget_Label.hpp"
#include "GuiGL/elWidget_base.hpp"
#include "ServiceMonitor/Factory.hpp"
#include "common/Settings.hpp"

using namespace el3D;
using namespace common;

namespace LarryScanner
{
PreviewWindow::PreviewWindow(
    el3D::GuiGL::elWidget_base* const parent,
    el3D::GuiGL::elWidget_base*       stickyWindowSource ) noexcept
    : parent( parent ), stickyWindowSource( stickyWindowSource )
{
    this->window =
        this->parent->Add< GuiGL::elWidget_base >( "LarryScannerPreview" );
    this->window->SetStickToWidget(
        this->stickyWindowSource, GuiGL::verticalPosition_t::VPOSITION_CENTER,
        GuiGL::horizontalPosition_t::HPOSITION_RIGHT );
    this->window->SetPositionPointOfOrigin(
        GuiGL::positionCorner_t::POSITION_CENTER );
    this->borderSize = this->window->GetBorderSize();
    this->window->SetBorderSize( glm::vec4{ 0.0f } );
    this->window->SetSize( { this->window->GetSize().x, 0.0f } );

    this->header = this->window->Add< GuiGL::elWidget_Label >( "Section" );
    this->headerDetail =
        this->window->Add< GuiGL::elWidget_Label >( "Section" );
    this->headerDetail->SetPosition( { 0.0, this->header->GetFontHeight() } );

    this->content = this->window->Add< GuiGL::elWidget_base >();
    this->content->SetPosition( { 0.0, this->GetRequiredHeaderHeight() } );
    this->content->SetStickToParentSize( { 0.0, -1.0 } );
}

void
PreviewWindow::Update() noexcept
{
    if ( this->previewService )
    {
        auto newSize = this->content->GetSize().y;
        this->previewService->Update();

        if ( this->previewHeight != newSize )
        {
            this->previewHeight = newSize;
            this->ResizeWindow( this->previewHeight +
                                this->GetRequiredHeaderHeight() );
        }
    }
}

void
PreviewWindow::Reset( const std::string& message,
                      const std::string& detail ) noexcept
{
    this->previewService.reset();
    this->header->SetText( message );
    this->headerDetail->SetText( detail );

    if ( message.empty() && detail.empty() )
    {
        this->ResizeWindow( 0.0f );
    }
    else
    {
        this->ResizeWindow( this->headerDetail->GetPosition().y +
                            this->headerDetail->GetSize().y +
                            ELEMENT_SPACING / 2.0f );
    }

    this->previewHeight = 0.0f;
}

void
PreviewWindow::GeneratePreview( const std::string& domain,
                                const std::string& service,
                                const std::string& entity ) noexcept
{
    this->previewService = ServiceMonitor::Factory::Create(
        ServiceMonitor::Service_CTor{ { domain, service, entity },
                                      this->window.Get(),
                                      this->content.Get() } );

    if ( this->previewService )
    {
        this->header->SetText( service );
        this->headerDetail->SetText( "" );
        this->window->SetDoRenderWidget( true );
        this->ResizeWindow( this->content->GetSize().y +
                            this->GetRequiredHeaderHeight() );
    }
    else
    {
        this->Reset( "unable to connect to", entity );
    }
}

float
PreviewWindow::GetRequiredHeaderHeight() const noexcept
{
    return this->header->GetPosition().y +
           static_cast< float >( this->header->GetFontHeight() ) +
           ELEMENT_SPACING / 2.0f;
}

void
PreviewWindow::ResizeWindow( const float height ) noexcept
{
    if ( height == 0.0f && this->window->GetBorderSize() != glm::vec4( 0.0f ) )
    {
        this->borderSize = this->window->GetBorderSize();
        this->window->SetBorderSize( glm::vec4( 0.0f ) );
    }
    else if ( height != 0.0f &&
              this->window->GetBorderSize() == glm::vec4( 0.0f ) )
    {
        this->window->SetBorderSize( this->borderSize );
    }


    this->window->ResizeTo( { this->window->GetSize().x, height } );
}


} // namespace LarryScanner
