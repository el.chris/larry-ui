#include "common/ServiceDescriptionGenerator.hpp"

namespace common
{
ServiceDescription
ServiceDescriptionGenerator::FindOtherInstances(
    const std::string& domain ) noexcept
{
    return {
        iox::capro::IdString_t( iox::cxx::TruncateToCapacity,
                                domain + ENTRY_SEPARATOR + EVENT_ANNOUNCE ),
        iox::capro::IdString_t( iox::capro::AnyInstanceString ),
        iox::capro::IdString_t( EVENT_ANNOUNCE ) };
}

ServiceDescription
ServiceDescriptionGenerator::ConnectToAnnouncementService(
    const std::string& domain, const std::string& entity ) noexcept
{
    return {
        iox::capro::IdString_t( iox::cxx::TruncateToCapacity,
                                domain + ENTRY_SEPARATOR + EVENT_ANNOUNCE ),
        iox::capro::IdString_t( iox::cxx::TruncateToCapacity, entity ),
        iox::capro::IdString_t( EVENT_ANNOUNCE ) };
}

ServiceDescription
ServiceDescriptionGenerator::ConnectToServiceInfo(
    const service_t& service ) noexcept
{
    return {
        iox::capro::IdString_t( iox::cxx::TruncateToCapacity, service.domain ),
        iox::capro::IdString_t( iox::cxx::TruncateToCapacity, service.service ),
        iox::capro::IdString_t( iox::cxx::TruncateToCapacity,
                                service.entity + ENTRY_SEPARATOR +
                                    EVENT_INFO ) };
}

ServiceDescription
ServiceDescriptionGenerator::ConnectToServiceCommand(
    const service_t& service ) noexcept
{
    return {
        iox::capro::IdString_t( iox::cxx::TruncateToCapacity, service.domain ),
        iox::capro::IdString_t( iox::cxx::TruncateToCapacity, service.service ),
        iox::capro::IdString_t( iox::cxx::TruncateToCapacity,
                                service.entity + ENTRY_SEPARATOR +
                                    EVENT_COMMAND ) };
}


} // namespace common
