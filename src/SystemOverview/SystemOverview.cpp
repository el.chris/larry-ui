#include "SystemOverview/SystemOverview.hpp"

#include "3Delch/3Delch.hpp"
#include "GuiGL/elWidget_Label.hpp"
#include "GuiGL/elWidget_base.hpp"

using namespace el3D;

namespace SystemOverview
{
SystemOverview::SystemOverview(
    el3D::GuiGL::elWidget_base* const parent ) noexcept
    : window( parent->Add< GuiGL::elWidget_base >( "SystemOverview" ) )
{
    this->framesPerSecond.SetUp( window.Get(),
                                 "FPS:", common::ELEMENT_SPACING / 2.0f );
    this->framesPerSecond.value->SetPositionPointOfOrigin(
        GuiGL::POSITION_TOP_LEFT );
    this->framesPerSecond.value->SetPosition(
        { 55.0f, this->framesPerSecond.value->GetPosition().y } );

    this->connection.SetUp(
        window.Get(), "Connection:", common::ELEMENT_SPACING / 2.0f, 125.0f );
    this->connection.value->SetPositionPointOfOrigin(
        GuiGL::POSITION_TOP_LEFT );
    this->connection.value->SetPosition(
        { 250.0f, this->connection.value->GetPosition().y } );
    this->Disconnect();
}

void
SystemOverview::Update() noexcept
{
    this->framesPerSecond.value->SetText( std::to_string(
        static_cast< uint64_t >( _3Delch::_3Delch::GetInstance()
                                     ->Window()
                                     ->GetFramesPerSecond() ) ) );
}

void
SystemOverview::Disconnect() noexcept
{
    this->connection.value->SetText( "disconnected" );
    this->connection.value->SetClassName( "Bad", false );
}

void
SystemOverview::Connect( const std::string& domain,
                         const std::string& entity ) noexcept
{
    this->connection.value->SetText( entity + "@" + domain );
    this->connection.value->SetClassName( "Good", false );
}


} // namespace SystemOverview
