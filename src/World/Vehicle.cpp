#include "World/Vehicle.hpp"

#include "OpenGL/types.hpp"
#include "common/ServiceDescriptionGenerator.hpp"

using namespace el3D;
using namespace common;

namespace World
{
Vehicle::Vehicle( el3D::_3Delch::elScene &scene ) noexcept
    : Object( scene, ObjectType::Vehicle )
{
    this->model =
        scene.AddObjectFromFile< OpenGL::RenderTarget::Forward >( MODEL, 0.1f )
            .OrElse( [] {
                LOG_FATAL( 0 )
                    << "unable to find entity model: \'" << MODEL << "\'";
                std::terminate();
            } )
            .GetValue();

    this->model->SetPosition( { 0.7, -0.1, 0.0 } );
    this->model->Rotate( this->rotationAxis, this->rotationSpeed );
    this->model->GetParentObject().SetCullFace( 0, OpenGL::CullFace::Disable );
}

void
Vehicle::UpdateImplementation() noexcept
{
    if ( this->geometricState && this->isConnected )
    {
        this->geometricState->take().and_then(
            [&]( iox::popo::Sample< const serviceData::trackingCamera_t >
                     &state ) {
                double angle = 2.0 * acos( state->rotation.w );
                double x     = state->rotation.x / sin( angle / 2.0 );
                double y     = state->rotation.y / sin( angle / 2.0 );
                double z     = state->rotation.z / sin( angle / 2.0 );

                this->model->SetRotation( { x, y, z, angle } );
            } );
    }
}

void
Vehicle::ConnectImplementation( const std::string &domain,
                                const std::string &entity ) noexcept
{
    this->model->RotateTo( { 0.0, 1.0, 0.0, 3.0 * 3.1415 / 2.0 }, 1.0,
                           animation::AnimationVelocity::Relative,
                           [=] { this->isConnected = true; } );
    this->model->ResizeTo( glm::vec3{ this->scaling }, 1.0,
                           animation::AnimationVelocity::Relative );
    this->model->MoveTo( { 2.0, -1.0, 0.0 }, 1.0,
                         animation::AnimationVelocity::Relative );

    this->geometricState.emplace(
        ServiceDescriptionGenerator::ConnectToServiceInfo(
            { domain, "TrackingCamera", entity } ),
        iox::popo::SubscriberOptions{ 1U, 0U } );
    this->geometricState->subscribe();
}

void
Vehicle::DisconnectImplementation() noexcept
{
    this->model->RotateTo( { 0.0, 1.0, 0.0, 0.5 }, 1.0,
                           animation::AnimationVelocity::Relative, [=] {
                               this->model->Rotate( this->rotationAxis,
                                                    this->rotationSpeed );
                           } );
    this->model->ResizeTo( { 1.0, 1.0, 1.0 }, 1.0,
                           animation::AnimationVelocity::Relative );
    this->model->MoveTo( { 7.0, -1.0, 0.0 }, 1.0,
                         animation::AnimationVelocity::Relative );

    this->isConnected = false;
    this->geometricState.reset();
}

} // namespace World
