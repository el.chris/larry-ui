#include "HeadsUpDisplay/HeadsUpDisplay.hpp"

#include "GuiGL/elWidget_base.hpp"
#include "common/ServiceDescriptionGenerator.hpp"

using namespace el3D;
using namespace common;

namespace HeadsUpDisplay
{
HeadsUpDisplay::HeadsUpDisplay( el3D::GuiGL::elWidget_base* const parent,
                                el3D::_3Delch::elScene* const scene ) noexcept
    : parent( parent )
{
    this->rootFrame = parent->Add< GuiGL::elWidget_base >();
    this->rootFrame->SetPositionPointOfOrigin(
        GuiGL::positionCorner_t::POSITION_CENTER );
    this->rootFrame->SetSize( { 0, 0 } );
    this->rootFrame->SetIsAlwaysOnBottom( true );

    this->wheelSpeed =
        this->rootFrame->Add< GuiGL::elWidget_ShaderAnimation >();
    this->wheelSpeed->SetClassName( "WheelSpeed", true );
    this->wheelSpeed->SetDisableEventHandling( true );
    this->wheelSpeed->SetUp( SHADER_FILE, "default", true, { { "" } }, 1, 1,
                             false );
    this->wheelSpeed->SetIsAlwaysOnBottom( true );
    this->wheelSpeed->SetDoInvertTextureCoordinates( false );

    this->wheelSpeedShader = &this->wheelSpeed->GetShaderTexture()->GetShader();
    this->wheelSpeedShader->GetUniformLocation( UNIFORM_NAME )
        .AndThen( [&]( const auto& r ) { this->wheelSpeedUniform = r; } )
        .OrElse( [] {
            LOG_ERROR( 0 ) << "unable to setup " << SHADER_FILE
                           << " for HUD WheelSpeed usage";
        } );
}

HeadsUpDisplay::~HeadsUpDisplay()
{
}

void
HeadsUpDisplay::Connect( const std::string& domain,
                         const std::string& entity ) noexcept
{
    this->rootFrame->ResizeTo( this->parent->GetSize() );

    this->wheelSpeedInfo.emplace(
        ServiceDescriptionGenerator::ConnectToServiceInfo(
            { domain, "Drive", entity } ),
        iox::popo::SubscriberOptions{ 1U, 0U } );
    this->wheelSpeedInfo->subscribe();
}

void
HeadsUpDisplay::Disconnect() noexcept
{
    this->rootFrame->ResizeTo( { 0, 0 } );

    this->wheelSpeedInfo.reset();
}

void
HeadsUpDisplay::Update() noexcept
{
    if ( this->wheelSpeedInfo )
    {
        this->wheelSpeedInfo->take().and_then(
            [&]( iox::popo::Sample< const serviceData::drive_t >& sample ) {
                this->SetWheelSpeed( { sample->left, sample->right } );
            } );
    }
}

void
HeadsUpDisplay::SetWheelSpeed( const glm::vec2& v ) noexcept
{
    if ( v == this->wheelSpeedCurrentValue ) return;

    if ( this->wheelSpeedAnimationID ==
         GuiGL::elWidget_ShaderAnimation::INVALID_ANIMATION_ID )
        this->wheelSpeedAnimationID = this->wheelSpeed->AnimateUniform(
            this->wheelSpeedUniform, this->wheelSpeedCurrentValue,
            glm::vec2( v.y, v.x ) / 100.0f, this->wheelSpeedAnimationSpeed );
    else
        this->wheelSpeedAnimationID = this->wheelSpeed->UpdateAnimatedUniform(
            this->wheelSpeedAnimationID, this->wheelSpeedUniform,
            glm::vec2( v.y, v.x ) / 100.0f, this->wheelSpeedAnimationSpeed );

    this->wheelSpeedCurrentValue = v;
}

} // namespace HeadsUpDisplay
