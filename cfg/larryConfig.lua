TRUE = 1
FALSE = 0

global = {
    log = {
        file = "logfile.log",
        level = 3, -- 3 = error
    },

    shaderIncludePath = {
        "build/_deps/3delch-src/",
        "build/_deps/3delch-src/glsl",
        "build/_deps/3delch-src/glsl/gui"
    },

    window = {
        name = "3Delch",
        size = {1680, 1050},
        openGLVersion = {4, 4},
        useOpenGLCoreProfile = TRUE,
        showMouseCursor = FALSE,
        relativeMouseMode = TRUE,
        windowGrabMouse = TRUE,
        enableVSync = FALSE,
        dpiScaling = 2.0,           -- for 4k display use either 2.0 dpiScaling or multiSamplingFactor = 0.5
        multiSamplingFactor = 1.0, -- 0.5 double pixel size
    },

    integratedConsole = {
        key = "F12",
    },

    camera = {
        factor = 0.5,
        speedInMeterPerSecond = 5.0,
        keys = {
            forward     = "w",
            backward    = "s",
            left        = "a",
            right       = "d",
            up          = "r",
            down        = "f",
            rollLeft    = "q",
            rollRight   = "e",
        },
    },

    scenes = {
        main = {
            light = {
                maxDirectionalLights            = 2,
                shadowMapSize_DirectionalLight  = {16384, 16384},
                shadowMapSize_PointLight        = {2048, 2048},
                shaderFile_deferred             = "glsl/Light/deferredRenderer.glsl"
            },

            nonVertexObject = {
                shaderFile_forward      = "glsl/NonVertexObject/forwardRenderer.glsl"
            },

            vertexObject = {
                shaderFile_forward      = "glsl/VertexObject/forwardRenderer.glsl",
                shaderFile_deferred     = "glsl/VertexObject/deferredRenderer.glsl"
            },

            unifiedVertexObject = {
                shaderFile_deferred     = "glsl/UnifiedVertexObject/deferredRenderer.glsl",
                shaderFile_forward      = "glsl/UnifiedVertexObject/forwardRenderer.glsl",
                textureSize             = {512, 512, 28},
            },

            instancedObject = {
                shaderFile_deferred     = "glsl/InstancedObject/deferredRenderer.glsl",
            },


            passThroughShader = {
                shaderFile_deferred     = "glsl/passThrough.glsl",
            },

            final = {
                shaderFile  = "glsl/finalRenderer.glsl",
            },

            forward = {
                defaultShader = {
                    file    = "glsl/forwardRenderer_VertexObject.glsl",
                    group   = "default",
                },
            },

            skybox = {
                shader = {
                    file    = "glsl/forwardRenderer_Skybox.glsl",
                    group   = "default",
                },
                textures = {
                    "data/textures/sb1-5.jpg", "data/textures/sb1-1.jpg",
                    "data/textures/sb1-2.jpg", "data/textures/sb1-4.jpg",
                    "data/textures/sb1-3.jpg", "data/textures/sb1-6.jpg"
                },
                color = { 0.2, 0.2, 0.2, 1.0 }
            },

            gui = {
                shaderFile_gui  = "glsl/gui/standard.glsl",
                theme = "cfg/larryTheme.lua",
                --theme = "cfg/guiTheme.lua",
            },

            postEffects = {
                ambientOcclusion = {
                    gtao = {
                        scaling = 1.0,
                        gtaoShader = {
                            file    = "glsl/PostProcessing/gtao.glsl",
                            group   = "default",
                        },
                        blurShader = {
                            file    = "glsl/PostProcessing/gtaoSpatialBlur.glsl",
                            group   = "default",
                        },
                        finalShader = {
                            file    = "glsl/PostProcessing/gtaoFinal.glsl",
                            group   = "default",
                        },
 
                    },
                },
                glow = {
                    gui = {
                        scaling = 1.0,
                        passes = 2,
                        intensity = 1.0,
                        size = 4,
                        shader = {
                            file    = "glsl/PostProcessing/gaussianBlur.glsl",
                            group   = "default",
                        },
                        shaderResize = {
                            file    = "glsl/PostProcessing/maxValueBlur.glsl",
                            group   = "default",
                        },
                     },
                    light = {
                        scaling = 0.125,
                        passes = 1,
                        intensity = 1.0,
                        size = 3,
                        shader = {
                            file    = "glsl/PostProcessing/gaussianBlur.glsl",
                            group   = "default",
                        },
                        shaderResize = {
                            file    = "glsl/PostProcessing/maxValueBlur.glsl",
                            group   = "default",
                        },                    
                    },
                },
            },
        },
    },
}
