window_ServiceMonitor = {
    default = {0.0, 0.0, 0.0, 0.0,      -- default color
               0.0, 0.0, 0.0, 0.0},     -- accent color
    inactive = {0.0, 0.0, 0.0, 0.0,      -- default color
                0.0, 0.0, 0.0, 0.0},     -- accent color
    hover =    {0.0, 0.0, 0.0, 0.0,      -- default color
                0.0, 0.0, 0.0, 0.0},     -- accent color
    clicked =  {0.0, 0.0, 0.0, 0.0,      -- default color
                0.0, 0.0, 0.0, 0.0},     -- accent color
    selected = {0.0, 0.0, 0.0, 0.0,      -- default color
                0.0, 0.0, 0.0, 0.0},     -- accent color
    size = {325, 1200},
    borderSize = {0, 0, 0, 0},
    borderEventIncrease = {0, 0, 0, 0},
    scrollbarWidth = {0, 0, 0, 10},
    scrollbarEnabled = {0, 0, 0, 0},

    isResizable = FALSE,
    isMovable = FALSE,

    button = {
        default = {0.0, 0.0, 0.03, 0.6,      -- default color
                   0.9, 0.6, 0.2, 0.0},     -- accent color
        inactive = {0.1, 0.1, 0.1, 0.4,      -- default color
                    0.0, 0.0, 0.0, 0.0},     -- accent color
        hover =    {0.0, 0.0, 0.03, 0.6,      -- default color
                    0.9, 0.6, 0.2, 0.6},     -- accent color
        clicked =  {0.0, 0.03, 0.03, 0.6,      -- default color
                    0.9, 0.6, 0.2, 1.0},     -- accent color
        selected = {0.0, 0.0, 0.03, 0.6,      -- default color
                    0.9, 0.6, 0.2, 0.8},     -- accent color

        size = {140, 30},
        borderSize = {0, 0, 3, 3},

        isResizable = FALSE,
        isUnSelectable = TRUE,
        isMovable = FALSE,

        label = deepcopy(label),
    },

    positionPointOfOrigin = 1, -- TOP_CENTER
}

base_ServiceMonitorInfoFrame = {
    default = {0.0, 0.0, 0.0, 0.2,
               0.0, 1.0, 0.7, 0.0},
    hover   = {0.0, 0.0, 0.0, 0.2,
               0.0, 0.6, 0.9, 0.0},
    clicked = {0.0, 0.0, 0.0, 0.2,
               0.0, 0.6, 0.9, 0.0},
    selected = {0.0, 0.0, 0.0, 0.2,
               0.0, 0.6, 0.9, 0.0},
    inactive = {0.0, 0.0, 0.0, 0.2,
               0.0, 0.6, 0.9, 0.0},
    borderSize = {0, 3, 3, 3},
    disableEventHandling = TRUE,
}

label_Key = deepcopy{label}
label_Key.default = {0.8, 0.9, 1.0, 0.0,
                         0.0, 0.0, 0.0, 0.0} -- alpha value MUST always be zero
label_Key.disableEventHandling = TRUE
label_Key.fontSize = label.fontSize

label_Value = deepcopy{label}
label_Value.default = {0.0, 0.8, 1.0, 0.0,
                         0.0, 0.0, 0.0, 0.0} -- alpha value MUST always be zero
label_Value.disableEventHandling = TRUE
label_Value.fontSize = label.fontSize

label_Good = deepcopy{label}
label_Good.default = {0.6, 1.0, 0.8, 0.0,
                         0.0, 0.0, 0.0, 0.0} -- alpha value MUST always be zero
label_Good.disableEventHandling = TRUE
label_Good.fontSize = label.fontSize

label_Bad = deepcopy{label}
label_Bad.default = {1.0, 0.5, 0.4, 0.0,
                         0.0, 0.0, 0.0, 0.0} -- alpha value MUST always be zero
label_Bad.disableEventHandling = TRUE
label_Bad.fontSize = label.fontSize

label_Indifferent = deepcopy{label}
label_Indifferent.default = {1.0, 0.8, 0.6, 0.0,
                         0.0, 0.0, 0.0, 0.0} -- alpha value MUST always be zero
label_Indifferent.disableEventHandling = TRUE
label_Indifferent.fontSize = label.fontSize


base_SystemOverview = {
    default = {0.0, 0.0, 0.0, 0.4,
               0.0, 0.6, 0.9, 0.4},
    size = {800, 30},
    borderSize = {0, 1, 1, 1},
    positionPointOfOrigin = 5, -- TOP_CENTER
    disableEventHandling = TRUE,
}


window_SystemInfo = deepcopy(window)
window_SystemInfo.size = {350, 500}
window_SystemInfo.position = {10, 10}
window_SystemInfo.scrollbarEnabled = {0, 0, 0, 0}
window_SystemInfo.positionPointOfOrigin = 1 -- TOP_RIGHT
window_SystemInfo.isResizable = FALSE
window_SystemInfo.isMovable = FALSE
window_SystemInfo.disableEventHandling = TRUE
window_SystemInfo.label_Heading = deepcopy{label}
window_SystemInfo.label_Heading.default = {0.8, 0.5, 0.5, 0.0,
                         0.0, 0.0, 0.0, 0.0} -- alpha value MUST always be zero
window_SystemInfo.label_Heading.disableEventHandling = TRUE

label_Section = deepcopy{label}
label_Section.default = {1.0, 0.8, 0.4, 0.0,
                         0.0, 0.0, 0.0, 0.0} -- alpha value MUST always be zero
label_Section.disableEventHandling = TRUE
label_Section.fontSize = label.fontSize


base_LarryScanner = {
    default = window.default,
    inactive = window.inactive,
    hover = window.hover,
    clicked = window.clicked,
    selected = window.selected,

    isResizable = FALSE,
    isMovable = FALSE,
    position = {0, 0},
    positionPointOfOrigin = 4, -- CENTER
    scrollbarEnabled = {0, 0, 0, 0},
    borderSize = window.borderSize,
}

base_LarryScannerPreview = deepcopy(base)
base_LarryScannerPreview.size = { 325.0, 100.0}
base_LarryScannerPreview.isResizable = FALSE
base_LarryScannerPreview.isMovable = FALSE
base_LarryScannerPreview.default = window.default
base_LarryScannerPreview.hover = window.hover
base_LarryScannerPreview.clicked = window.clicked
base_LarryScannerPreview.selected = window.selected
base_LarryScannerPreview.inactive = window.inactive
base_LarryScannerPreview.borderSize = window.borderSize



base_CameraFeed = {
    size = {528, 297},
    position = {10, 10},
    positionPointOfOrigin = 1, -- TOP_RIGHT
}

shaderAnimation_WheelSpeed = deepcopy(shaderAnimation)
shaderAnimation_WheelSpeed.positionPointOfOrigin = 4 -- CENTER
shaderAnimation_WheelSpeed.size = {1400, 1400}


