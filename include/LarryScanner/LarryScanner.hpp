#pragma once

#include "GuiGL/GuiGL.hpp"
#include "LarryScanner/PreviewWindow.hpp"
#include "iceoryx_posh/popo/subscriber.hpp"
#include "serviceData/identifier_t.hpp"

#include <glm/glm.hpp>
#include <optional>

namespace LarryScanner
{
class LarryScanner
{
  public:
    struct connectionInfo_t
    {
        std::string                domain;
        std::string                entity;
        std::vector< std::string > services;
    };

    using connectCallback_t = std::function< void( const connectionInfo_t& ) >;
    using disconnectCallback_t = std::function< void() >;

    LarryScanner( el3D::GuiGL::elWidget_base* const parent,
                  const connectCallback_t&          connectCallback,
                  const disconnectCallback_t& disconnectCallback ) noexcept;

    void
    Update() noexcept;

  private:
    enum class ConnectionType
    {
        DirectConnect,
        ConnectThroughScan,
    };

    enum class State
    {
        PerformServiceScan,
        EstablishDirectConnection,
        Undefined
    };

    void
    ScanButtonCallback() noexcept;
    void
    SelectRobotInstanceCallback() noexcept;
    void
    SelectServiceCallback() noexcept;
    void
    ConnectButtonCallback( const ConnectionType connectionType ) noexcept;
    void
    DisconnectButtonCallback() noexcept;
    void
    ShowScannerWidgets( const bool ) noexcept;
    void
    ConnectToAnnouncementService(
        const std::string& entity, const std::string& domain,
        const std::function< void() >& onUpdate ) noexcept;
    bool
    UpdateAvailableServices() noexcept;

  private:
    std::string eventInfoID = "info";

    el3D::GuiGL::elWidget_base*       parent;
    el3D::GuiGL::elWidget_base_ptr    window;
    el3D::GuiGL::elWidget_Entry_ptr   entityEntry;
    el3D::GuiGL::elWidget_Button_ptr  directConnectButton;
    el3D::GuiGL::elWidget_Label_ptr   domainLabel;
    el3D::GuiGL::elWidget_Entry_ptr   domainEntry;
    el3D::GuiGL::elWidget_Button_ptr  scanButton;
    el3D::GuiGL::elWidget_Button_ptr  exitButton;
    el3D::GuiGL::elWidget_Label_ptr   servicesFound;
    el3D::GuiGL::elWidget_Listbox_ptr entities;
    el3D::GuiGL::elWidget_Button_ptr  connect;
    el3D::GuiGL::elWidget_Button_ptr  disconnect;
    el3D::GuiGL::elWidget_Table_ptr   serviceDetails;

    PreviewWindow preview;

    glm::vec2 startWindowSize{ 310, 0 };
    std::optional< iox::popo::TypedSubscriber< serviceData::identifier_t > >
                               announcementService;
    std::string                selectedRobot;
    std::vector< std::string > availableServices;
    connectCallback_t          connectCallback;
    disconnectCallback_t       disconnectCallback;
    std::function< void() >    serviceUpdateCallback;
};
} // namespace LarryScanner
