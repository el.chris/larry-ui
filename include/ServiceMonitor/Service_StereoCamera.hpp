#pragma once

#include "GuiGL/GuiGL.hpp"
#include "ServiceMonitor/Service_base.hpp"

namespace ServiceMonitor
{
class Service_StereoCamera : public Service_base
{
  public:
    Service_StereoCamera( const Service_CTor& args ) noexcept;

    void
    Update() noexcept override;

  private:
    el3D::GuiGL::elWidget_Label_ptr distanceToCenter;
};
} // namespace ServiceMonitor
