#pragma once

#include "GuiGL/GuiGL.hpp"
#include "buildingBlocks/elCircularBuffer.hpp"
#include "common/ServiceDescriptionGenerator.hpp"
#include "iceoryx_posh/popo/subscriber.hpp"

#include <string>

namespace ServiceMonitor
{
struct Service_CTor
{
    common::service_t           service;
    el3D::GuiGL::elWidget_base* header{ nullptr };
    el3D::GuiGL::elWidget_base* infoFrame{ nullptr };
};


class Service_base
{
  public:
    Service_base( const Service_CTor& args ) noexcept;
    virtual ~Service_base();

    bool
    IsConnected() const;
    bool
    HasServiceDescripton( const common::service_t& service ) const noexcept;

    virtual void
    Update() noexcept;

  protected:
    iox::popo::UntypedSubscriber subscriber;
    el3D::GuiGL::elWidget_base*  header{ nullptr };
    el3D::GuiGL::elWidget_base*  infoFrame{ nullptr };
};
} // namespace ServiceMonitor
