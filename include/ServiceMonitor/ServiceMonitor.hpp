#pragma once

#include "GuiGL/elWidget_Button.hpp"
#include "GuiGL/elWidget_Window.hpp"
#include "GuiGL/elWidget_base.hpp"
#include "ServiceMonitor/Service_base.hpp"

#include <vector>

namespace ServiceMonitor
{
class ServiceMonitor
{
  public:
    ServiceMonitor( el3D::GuiGL::elWidget_base* const parent,
                    const std::string&                domain,
                    const std::string&                instance ) noexcept;

    void
    AddService( const std::string& service ) noexcept;
    void
    RemoveService( const std::string& service ) noexcept;

    void
    Update() noexcept;

  private:
    void
    Reshape() noexcept;

  private:
    static constexpr uint64_t HEADER_HEIGHT = 30;

    el3D::GuiGL::elWidget_Window_ptr window;
    std::string                      domain;
    std::string                      instance;

    struct ServiceEntry_t
    {
        el3D::GuiGL::elWidget_Button* previousHeader{ nullptr };

        el3D::GuiGL::elWidget_Button_ptr header;
        el3D::GuiGL::elWidget_base_ptr   infoFrame;
        std::unique_ptr< Service_base >  service{ nullptr };
        bool                             isOpen{ false };
    };

    std::vector< std::unique_ptr< ServiceEntry_t > > services;
};
} // namespace ServiceMonitor
