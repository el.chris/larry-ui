#pragma once

#include "OpenGL/elTexture_2D.hpp"
#include "ServiceMonitor/Service_base.hpp"
#include "common/KeyValuePair.hpp"
#include "utils/elThroughputMeasurement.hpp"

#include <optional>

namespace ServiceMonitor
{
class Service_Camera : public Service_base
{
  public:
    Service_Camera( const Service_CTor& args ) noexcept;

    void
    Update() noexcept override;

  private:
    el3D::GuiGL::elWidget_Label_ptr                  frameCounterLabel;
    el3D::utils::elThroughputMeasurement< uint64_t > frameCounter{
        el3D::units::Time::MilliSeconds( 250.0 ), 0u };

    std::optional< el3D::OpenGL::elTexture_2D > cameraImageTexture;
    el3D::GuiGL::elWidget_Image_ptr             cameraImage;
    common::KeyValuePair                        resolution;
    common::KeyValuePair                        angle;
    common::KeyValuePair                        imageSize;
    common::KeyValuePair                        fps;
};
} // namespace ServiceMonitor
