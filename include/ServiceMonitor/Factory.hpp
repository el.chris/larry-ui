#pragma once

#include "ServiceMonitor/Service_base.hpp"

#include <memory>
#include <string>

namespace ServiceMonitor
{
class Factory
{
  public:
    static std::unique_ptr< Service_base >
    Create( const Service_CTor& ctor ) noexcept;
};
} // namespace ServiceMonitor
