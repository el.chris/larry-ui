#pragma once

#include "GuiGL/GuiGL.hpp"
#include "common/KeyValuePair.hpp"

namespace SystemOverview
{
class SystemOverview
{
  public:
    SystemOverview( el3D::GuiGL::elWidget_base* const parent ) noexcept;

    void
    Update() noexcept;
    void
    Disconnect() noexcept;
    void
    Connect( const std::string& domain, const std::string& entity ) noexcept;

  private:
    el3D::GuiGL::elWidget_base_ptr window;
    common::KeyValuePair           framesPerSecond;
    common::KeyValuePair           connection;
};
} // namespace SystemOverview
