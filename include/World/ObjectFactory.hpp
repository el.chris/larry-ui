#pragma once

#include "3Delch/elScene.hpp"
#include "World/Object.hpp"

#include <vector>

namespace World
{
class ObjectFactory
{
  public:
    template < typename T >
    using objectPtr = std::unique_ptr< T, std::function< void( T * ) > >;

    ObjectFactory( el3D::_3Delch::elScene &scene ) noexcept;

    template < typename T, typename... Targs >
    objectPtr< T >
    CreateObject( Targs &&...args ) noexcept;

  protected:
    void
    UpdateChildren() noexcept;
    void
    ConnectChildren( const std::string &domain,
                     const std::string &entity ) noexcept;
    void
    DisconnectChildren() noexcept;

  private:
    void
    RemoveObject( void *rawObjectPtr ) noexcept;

    std::vector< std::unique_ptr< Object > > objects;

    el3D::_3Delch::elScene *scene = nullptr;
};
} // namespace World

#include "World/ObjectFactory.inl"
