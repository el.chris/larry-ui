#pragma once

#include "3Delch/elScene.hpp"

namespace World
{
enum class ObjectType
{
    Vehicle
};

class Object
{
  public:
    Object( el3D::_3Delch::elScene& scene, const ObjectType type ) noexcept;

    void
    Update() noexcept;
    void
    Connect( const std::string& domain, const std::string& entity ) noexcept;
    void
    Disconnect() noexcept;

  protected:
    virtual void
    UpdateImplementation() noexcept = 0;
    virtual void
    ConnectImplementation( const std::string& domain,
                           const std::string& entity ) noexcept = 0;
    virtual void
    DisconnectImplementation() noexcept = 0;

    el3D::_3Delch::elScene&
    GetScene() noexcept;

  private:
    ObjectType              type;
    el3D::_3Delch::elScene* scene = nullptr;
};
} // namespace World
