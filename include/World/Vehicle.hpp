#pragma once

#include "World/Object.hpp"
#include "iceoryx_posh/popo/subscriber.hpp"
#include "serviceData/trackingCamera_t.hpp"

namespace World
{
class Vehicle : public Object
{
  public:
    Vehicle( el3D::_3Delch::elScene &scene ) noexcept;

  protected:
    virtual void
    UpdateImplementation() noexcept override;
    virtual void
    ConnectImplementation( const std::string &domain,
                           const std::string &entity ) noexcept override;
    virtual void
    DisconnectImplementation() noexcept override;

  private:
    static constexpr char MODEL[] = "data/Larry-T1-Model.obj";
    el3D::HighGL::elGeometricObject_UnifiedVertexObject::compoundObject_t
        *                       model;
    glm::vec3                   rotationAxis{ 0.0, 1.0, 0.0 };
    el3D::animation::velocity_t rotationSpeed = 0.125f;
    float                       scaling       = 0.0625f;
    std::optional< iox::popo::TypedSubscriber< serviceData::trackingCamera_t > >
         geometricState;
    bool isConnected{ false };
};
} // namespace World
