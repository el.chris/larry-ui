namespace World
{

template < typename T, typename... Targs >
inline ObjectFactory::objectPtr< T >
ObjectFactory::CreateObject( Targs &&...args ) noexcept
{
    T *newObject = new T( std::forward< Targs >( args )... );
    this->objects.emplace_back( newObject );

    return objectPtr< T >( newObject,
                           [this]( T *ptr ) { this->RemoveObject( ptr ); } );
}
} // namespace World
