#pragma once

#include <cstdint>
#include <string>

namespace common
{
static constexpr uint64_t FLOAT_PRECISION = 4;

std::string
DoubleToString( const double v, const uint64_t length = FLOAT_PRECISION,
                const bool addSpaceIfPositive = false ) noexcept;
} // namespace common
