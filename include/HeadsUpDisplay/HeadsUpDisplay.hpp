#pragma once

#include "3Delch/elScene.hpp"
#include "GuiGL/GuiGL.hpp"
#include "GuiGL/elWidget_ShaderAnimation.hpp"
#include "HighGL/elMeshLoader.hpp"
#include "OpenGL/elShaderProgram.hpp"
#include "iceoryx_posh/popo/subscriber.hpp"
#include "serviceData/drive_t.hpp"

#include <string>

namespace HeadsUpDisplay
{
class HeadsUpDisplay
{
  public:
    HeadsUpDisplay( el3D::GuiGL::elWidget_base* const parent,
                    el3D::_3Delch::elScene* const     scene ) noexcept;

    // delete all copy/move stuff
    ~HeadsUpDisplay();

    void
    Connect( const std::string& domain, const std::string& entity ) noexcept;
    void
    Disconnect() noexcept;
    void
    Update() noexcept;

  private:
    void
    SetWheelSpeed( const glm::vec2& v ) noexcept;

  private:
    el3D::GuiGL::elWidget_base* parent{ nullptr };

    el3D::GuiGL::elWidget_base_ptr rootFrame;

    static constexpr char SHADER_FILE[] =
        "glsl/speed-element-shaderTexture.glsl";
    static constexpr char UNIFORM_NAME[] = "wheelSpeed";

    std::optional< iox::popo::TypedSubscriber< serviceData::drive_t > >
                                              wheelSpeedInfo;
    el3D::GuiGL::elWidget_ShaderAnimation_ptr wheelSpeed;
    el3D::OpenGL::elShaderProgram*            wheelSpeedShader{ nullptr };
    GLint                                     wheelSpeedUniform = -1;
    glm::vec2                                 wheelSpeedCurrentValue{ 0, 0 };
    float                                     wheelSpeedAnimationSpeed = 4.0f;
    size_t                                    wheelSpeedAnimationID =
        el3D::GuiGL::elWidget_ShaderAnimation::INVALID_ANIMATION_ID;
};
} // namespace HeadsUpDisplay
